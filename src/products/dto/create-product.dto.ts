import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @MinLength(2)
  price: number;
}
